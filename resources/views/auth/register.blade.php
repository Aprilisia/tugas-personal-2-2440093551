@extends('template.main')

@section('container')
<form class="form w-100" novalidate="novalidate" id="kt_sign_up_form" method="POST" action="{{ route('register') }}">
	@csrf
	<div class="mb-10 text-center">
		<h1 class="text-dark mb-3">Create an Account</h1>
		<div class="text-gray-400 fw-bold fs-4">Already have an account?
		<a href="{{ route('login') }}" class="link-primary fw-bolder">Sign in here</a></div>
	</div>
	<div class="row fv-row mb-7">
		<label class="form-label fw-bolder text-dark fs-6">Name</label>
		<input id="name" type="text" class="form-control form-control-lg form-control-solid @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
	</div>
	<div class="fv-row mb-7">
		<label class="form-label fw-bolder text-dark fs-6">Email</label>
		<input id="email" type="email" class="form-control form-control-lg form-control-solid @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
	</div>
	<div class="mb-10 fv-row" data-kt-password-meter="true">
		<div class="mb-1">
			<label class="form-label fw-bolder text-dark fs-6">Password</label>
			<div class="position-relative mb-3">
				<input id="password" type="password" class="form-control form-control-lg form-control-solid @error('password') is-invalid @enderror" name="password" value="{{ old('password') }}" required autocomplete="email">
				<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
					<i class="bi bi-eye-slash fs-2"></i>
					<i class="bi bi-eye fs-2 d-none"></i>
				</span>
			</div>
			<div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
			</div>
		</div>
		<div class="text-muted">Use 10 or more characters with a mix of letters, numbers &amp; symbols.</div>
	</div>
	<div class="fv-row mb-5">
		<label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
        <input id="password-confirm" type="password" class="form-control form-control-lg form-control-solid" name="password_confirmation" required autocomplete="new-password">
	</div>
	<div class="text-center">
		<button type="button" id="kt_sign_up_submit" class="btn btn-lg btn-primary">
			<span class="indicator-label">Submit</span>
			<span class="indicator-progress">Please wait...
			<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
		</button>
	</div>
</form>
@endsection