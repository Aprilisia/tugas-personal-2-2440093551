@extends('template.main')

@section('container')

<form class="form w-100" novalidate="novalidate" id="kt_password_reset_form" method="POST" action="{{ route('password.email') }}">
	@csrf
	<div class="text-center mb-10">
		<h1 class="text-dark mb-3">Forgot Password ?</h1>
		<div class="text-gray-400 fw-bold fs-4">Enter your email to reset your password.</div>
	</div>
	@if (session('status'))
		<div class="alert alert-success alert-dismissible fade show" role="alert">
			<span id="error_message">{{ session('status') }}</span>
			<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
		</div>
	@endif
	<div class="fv-row mb-10">
		<label class="form-label fw-bolder text-gray-900 fs-6">Email</label>
		<input id="email" type="email" class="form-control form-control-solid @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
		@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>
	<div class="d-flex flex-wrap justify-content-center pb-lg-0">
		<button type="button" id="kt_password_reset_submit" class="btn btn-lg btn-primary fw-bolder me-4">
			<span class="indicator-label">Submit</span>
			<span class="indicator-progress">Please wait...
			<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
		</button>
		<a href="{{ route('login') }}" class="btn btn-lg btn-light-primary fw-bolder">Cancel</a>
	</div>
</form>
@endsection