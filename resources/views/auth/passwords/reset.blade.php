@extends('template.main')

@section('container')
<form class="form w-100" novalidate="novalidate" id="kt_new_password_form" method="POST" action="{{ route('password.update') }}">
	@csrf
	<input type="hidden" name="token" value="{{ $token }}">
	<div class="text-center mb-10">
		<h1 class="text-dark mb-3">Setup New Password</h1>
		<div class="text-gray-400 fw-bold fs-4">Already have reset your password ?
		<a href="{{ route('login') }}" class="link-primary fw-bolder">Sign in here</a></div>
	</div>
	<div class="fv-row mb-10">
		<label class="form-label fw-bolder text-gray-900 fs-6">Email</label>
		<input id="email" type="email" class="form-control form-control-solid @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>
		@error('email')
			<span class="invalid-feedback" role="alert">
				<strong>{{ $message }}</strong>
			</span>
		@enderror
	</div>
	<div class="mb-10 fv-row" data-kt-password-meter="true">
		<div class="mb-1">
			<label class="form-label fw-bolder text-dark fs-6">Password</label>
			<div class="position-relative mb-3">
				<input id="password" type="password" class="form-control form-control-lg form-control-solid @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
				<span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
					<i class="bi bi-eye-slash fs-2"></i>
					<i class="bi bi-eye fs-2 d-none"></i>
				</span>
			</div>
			@error('password')
				<span class="invalid-feedback" role="alert">
					<strong>{{ $message }}</strong>
				</span>
			@enderror
			<div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
				<div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
			</div>
		</div>
		<div class="text-muted">Use 10 or more characters with a mix of letters, numbers &amp; symbols.</div>
	</div>
	<div class="fv-row mb-10">
		<label class="form-label fw-bolder text-dark fs-6">Confirm Password</label>
		<input id="password-confirm" type="password" class="form-control form-control-lg form-control-solid" name="password_confirmation" required autocomplete="new-password">
	</div>
	<div class="text-center">
		<button type="button" id="kt_new_password_submit" class="btn btn-lg btn-primary fw-bolder">
			<span class="indicator-label">Submit</span>
			<span class="indicator-progress">Please wait...
			<span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
		</button>
	</div>
</form>
@endsection