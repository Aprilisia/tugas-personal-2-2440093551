<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Laravel</title>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<link href="<?= url('') ?>/css/plugins.bundle.css" rel="stylesheet" type="text/css" />
		<link href="<?= url('') ?>/css/style.bundle.css" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.10.2/font/bootstrap-icons.css">
	</head>
	<body id="kt_body" class="bg-body">
		<div class="d-flex flex-column flex-root">
            <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-color: #C0DEFF">
                <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
                    <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
						@yield('container')
                    </div>
                </div>
            </div>
		</div>
		<script>var hostUrl = "<?= url('') ?>/";</script>
		<script src="<?= url('') ?>/js/plugins.bundle.js"></script>
		<script src="<?= url('') ?>/js/scripts.bundle.js"></script>
		<script src="<?= url('') ?>/js/sign-in.js"></script>
		<script src="<?= url('') ?>/js/sign-up.js"></script>
		<script src="<?= url('') ?>/js/password-reset.js"></script>
		<script src="<?= url('') ?>/js/new-password.js"></script>
	</body>
</html>