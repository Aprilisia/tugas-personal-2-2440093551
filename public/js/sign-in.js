"use strict";
var KTSigninGeneral = (function () {
    var t, e, i;
    return {
        init: function () {
            (t = document.querySelector("#kt_sign_in_form")),
                (e = document.querySelector("#kt_sign_in_submit")),
                (i = FormValidation.formValidation(t, {
                    fields: {
                        email: { validators: { notEmpty: { message: "Email address is required" }, emailAddress: { message: "The value is not a valid email address" } } },
                        password: { validators: { notEmpty: { message: "The password is required" } } },
                    },
                    plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row" }) },
                }))

                let message = $('#error_message')
                if(message.text().includes('attempts')) {
                    $('input, button').prop('disabled', true)
                    let time = message.text().substring(45, message.text().indexOf(' seconds'))
                    console.log(time)
                    setInterval(() => {
                        time = time - 1;
                        message.text(`Too many login attempts. Please try again in ${time} seconds.`)
                        if(time == 0) {
                            $('.alert').remove()
                            $('input, button').prop('disabled', false)
                        }
                    }, 1000);
                }

                e.addEventListener("click", function (n) {
                    n.preventDefault()
                        i.validate().then(function (i) {
                            "Valid" == i
                                ? t.submit()
                                : Swal.fire({
                                      text: "Sorry, looks like there are some errors detected, please try again.",
                                      icon: "error",
                                      buttonsStyling: !1,
                                      confirmButtonText: "Ok, got it!",
                                      customClass: { confirmButton: "btn btn-primary" },
                                  });
                        });
                });
        },
    };
})();
KTUtil.onDOMContentLoaded(function () {
    KTSigninGeneral.init();
});
